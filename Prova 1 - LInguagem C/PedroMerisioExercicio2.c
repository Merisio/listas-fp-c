#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main (void)
{
    char opcao;
    float per=0, media;
    int i, j, tabasc, ccara=0, cmin=0, cdiv=0, soma=0, linha=0; //O "c" na frente das variaveis simboliza "contador". A variavel "tabasc" representa a tabela ASCII.

    do
    {
        printf ("Insira um caractere: ");
        setbuf (stdin, NULL);
        scanf ("%c", &opcao);

        if (opcao>='1' && opcao<='9')
        {
            tabasc = (int)opcao;

            for (i = tabasc; i>0; i--)
            {
                if (i!=1)
                    printf("%d*", i);

                else if (i == 1)
                    printf("%d \n", i);
            }
        }
        if ((opcao>='A' && opcao<='Z')||(opcao>='a' && opcao<='z'))
        {
            tabasc = (int)opcao;

            for (i=1; i<=tabasc; i++)
            {
                cdiv = 0;

                for (j=1; j<=tabasc; j++)
                {
                    if(i%j == 0)
                    {
                        cdiv++;
                    }
                }
                if (cdiv == 2)//Condi��o para os n�meros primos.
                {
                    printf("%d\t", i);
                    linha++;
                    soma = soma+i;
                }
                if (linha == 6)//Condi��o para que haja apenas 6 n�meros por linha.
                {
                    printf("\n");
                    linha = 0;
                }
            }
            printf ("\nSoma dos numeros primos: %d\n", soma);
        }
        if (opcao>='a' && opcao<='z')//Decis�o para o percentual de min�sculas.
            cmin++;
    }
    while (opcao!='0');

    per = (float)(100*cmin)/(ccara-1);

    printf ("O percentual de minusculas eh: %.2f", per);

    return 0;
}



