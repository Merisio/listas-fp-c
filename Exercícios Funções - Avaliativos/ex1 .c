/*1) Fazer uma fun��o que verifica se um n�mero � triangular. Um n�mero natural � triangular quando o
produto de tr�s n�meros naturais consecutivos for igual ao pr�prio n�mero. Por exemplo: 120 � triangular,
pois 4 * 5 * 6 = 120. Use essa fun��o para:*/

#include <stdio.h>
#include <stdlib.h>
#include "Funcoes.h"

int main (void)
{
    char rep, opcao;
    int num, inicio, fim;

    do
    {
        printf ("\n\n========================= OPCOES ==========================\n");
        printf ("1 - Verificar se o numero eh triangular.\n");
        printf ("2 - Mostrar quais numeros de um intervalo sao triangulares.\n");
        printf ("===========================================================\n");
        printf ("Selecione uma opcao: ");
        setbuf(stdin, NULL);
        scanf ("%c", &opcao);

        switch (opcao)
        {
            case '1':
                printf ("\nInsira um numero: ");
                scanf ("%d", &num);
                triangular(num);
                break;

            case '2':
                printf ("Informe o inicio do intervalo: ");
                scanf ("%d", &inicio);
                printf ("Informe o fim o intervalo: ");
                scanf ("%d", &fim);
                printf ("\nOs numeros triangulares no intervalo sao:\n\n");

                for (int x = inicio; x<=fim; x++)
                {
                    if ((interTria(x)) == 1)
                    {
                        printf("%d\t", x);
                    }
                    if (x == fim)
                    {
                        printf("\n");
                    }
                }
                break;
            default:
                printf ("Caractere invalido.");
        }
        printf ("\n\nDeseja repetir o programa? ");
        setbuf(stdin, NULL);
        scanf ("%c", &rep);

    }
    while (rep == 'S' || rep == 's');

    return 0;
}
