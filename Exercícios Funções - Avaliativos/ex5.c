/*5) Elaborar uma fun��o que recebe o n�mero de linhas, o n�mero de colunas, um determinado caractere e
desenha um quadrado ou ret�ngulo na tela utilizando o caractere.*/

#include <stdio.h>
#include <stdlib.h>
#include "Funcoes.h"

int main (void)
{
    int linha, col;
    char carac;

    printf ("Insira o numero de linhas: ");
    scanf ("%d", &linha);
    printf ("Insira o numero de colunas: ");
    scanf ("%d", &col);
    printf ("Insira o caractere desejado: ");
    setbuf (stdin, NULL);
    scanf ("%c", &carac);
    printf ("\n");

    caractere (carac, linha, col);

    return 0;
}
