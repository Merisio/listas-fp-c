/*4) Apresentar os n�meros entre 10 e 0, ou seja, em ordem decrescente, separados por tabula��o.*/

#include <stdio.h>
#include <stdlib.h>

int main (void)
{
    int num;

    for (num=10; num>=0; num--)
        printf ("%d\n", num);

    return 0;
}
