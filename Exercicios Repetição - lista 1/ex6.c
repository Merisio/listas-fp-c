#include <stdio.h>
#include <stdlib.h>

int main (void)
{
    int num1, num2, i, aux, soma=0, cont=0;
    char opcao;
    float media;

    do
    {
        printf ("Insira o inicio do intervalo: ");
        scanf ("%d", &num1);
        printf ("Insira o final do intervalo: ");
        scanf ("%d", &num2);
        printf ("Insira o valor do incremento: ");
        scanf ("%d", &i);

        if (num1>num2)
        {
            for (aux = num2; aux<=num1; aux = (aux+i))
            {
                printf ("\n%d", aux);
                if (aux%2!=0 && aux%35==0)
                {
                    printf ("\tImpar e divisivel por 35.", aux);
                    soma = soma + aux;
                    cont++;
                }
            }
            media = ((float)soma/cont);
            printf ("\n\nA media dos numeros impares e divisiveis por 35 eh: %.2f", media);
        }
        else if (num1<num2)
        {
            for (aux = num1; aux<=num2; aux = (aux+i))
            {
                printf ("\n%d", aux);
                if (aux%2!=0 && aux%35==0)
                {
                    printf ("\tImpar e divisivel por 35.", aux);
                    soma = soma + aux;
                    cont++;
                }
            }
            media = ((float)soma/cont);
            printf ("\n\nA media dos numeros impares e divisiveis por 35 eh: %.2f", media);
        }
        else
            printf ("\nNao existe intervalo.\n");

        printf ("\n\nFim do programa.\n");
        printf ("Deseja repetir?");
        setbuf (stdin, NULL);
        scanf ("%c", &opcao);
    }
    while (opcao == 'S' || opcao == 's');

    return 0;
}
