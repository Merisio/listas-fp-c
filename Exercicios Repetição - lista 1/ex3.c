/*3) Apresentar os n�meros entre 0 e 4, com intervalo de 0.25 entre eles, separados por tabula��o.*/

#include <stdio.h>
#include <stdlib.h>

int main (void)
{
    float num;

    for (num=0; num<=4; num=num+0.25)
        printf ("%.2f\n", num);

    return 0;
}
