/*5) Ler um n�mero maior que 2 e imprimir todos os pares entre 2 e o n�mero lido. Imprimir a soma dos pares,
o produto dos �mpares que s�o divis�veis por 9 e a m�dia de todos os n�meros do intervalo.*/

#include <stdio.h>
#include <stdlib.h>

int main (void)
{
    int num;
    printf ("Insira um valor maior que 2: ");
    scanf ("%d", &num);

    printf ("Os numeros pares sao:\n");
    do{
        if (num%2==0){
           printf ("%d\n", num);
        }
        num--;

    }while (num>=2);

return 0;
}
