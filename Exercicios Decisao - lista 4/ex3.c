/*3) Fa�a um programa que determine a data cronologicamente maior entre duas datas fornecidas pelo usu�rio. Cada
data deve ser composta por tr�s valores inteiros, em que o primeiro representa o dia, o segundo, o m�s e o terceiro, o
ano.*/

#include <stdio.h>
#include <stdlib.h>

int main (void)
{
    int d1, m1, a1, d2, m2, a2;

    printf ("Insira uma data:\n");
    printf ("Dia: ");
    scanf ("%d", &d1);
    printf ("Mes: ");
    scanf ("%d", &m1);
    printf ("Ano: ");
    scanf ("%d", &a1);

    printf ("Insira outra data:\n");
    printf ("Dia: ");
    scanf ("%d", &d2);
    printf ("Mes: ");
    scanf ("%d", &m2);
    printf ("Ano: ");
    scanf ("%d", &a2);

    if (a1>a2)
        printf ("\nA maior data eh %d/%d/%d.\n", d1, m1, a1);

    else if (a1<a2)
        printf ("\nA maior data eh %d/%d/%d.\n", d2, m2, a2);

    else if (a1==a2)
    {
        if (m1>m2)
            printf ("\nA maior data eh %d/%d/%d.\n", d1, m1, a1);

        else if (m1<m2)
            printf ("\nA maior data eh %d/%d/%d.\n", d2, m2, a2);

        else if (m1==m2)
        {
            if (d1>d2)
                printf ("\nA maior data eh %d/%d/%d.\n", d1, m1, a1);

            else if (d1<d2)
                printf ("\nA maior data eh %d/%d/%d.\n", d2, m2, a2);

            else if (d1==d2)
                printf ("\nAs datas sao iguais.\n");
        }
    }
    return 0;
}
