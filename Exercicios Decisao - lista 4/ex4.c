/*4) Fa�a um programa que receba o sal�rio de um funcion�rio e o c�digo correspondente ao seu cargo atual e mostre o
cargo, o valor do aumento e seu novo sal�rio. Os cargos est�o na tabela a seguir:*/

#include <stdio.h>
#include <stdlib.h>

int main (void)
{
    float sal;
    int opcao;

    printf ("Insira seu salario atual: ");
    scanf ("%f", &sal);
    printf ("Cargos:\n");
    printf ("1 - Escriturario\n");
    printf ("2 - Secretario\n");
    printf ("3 - Caixa\n");
    printf ("4 - Gerente\n");
    printf ("5 - Diretor\n");
    printf ("Selecione seu cargo: ");
    scanf ("%d", &opcao);

    switch (opcao)
    {
    case 1:
        printf ("\nValor do aumento: R$%.2f", sal*0.5);
        printf ("\nSeu salario com aumento: R$%.2f\n", sal*1.5);

    break;
    case 2:
        printf ("\nValor do aumento: R$%.2f", sal*0.35);
        printf ("\nSeu salario com aumento: R$%.2f\n", sal*1.35);

    break;
    case 3:
        printf ("\nValor do aumento: R$%.2f", sal*0.2);
        printf ("\nSeu salario com aumento: R$%.2f\n", sal*1.2);

    break;
    case 4:
        printf ("\nValor do aumento: R$%.2f", sal*0.1);
        printf ("\nSeu salario com aumento: R$%.2f\n", sal*1.1);

    break;
    case 5:
        printf ("\nVoce nao tera aumento.\n");

    break;
    }
    return 0;
}
