/*1) A contribui��o para o INSS � calculada da seguinte forma:
a) Sal�rio bruto at� tr�s sal�rios m�nimos = INSS 8%.
b) Sal�rio bruto acima de tr�s sal�rios m�nimos = INSS 10%.
c) Para as contribui��es maiores que o sal�rio m�nimo, considerar a import�ncia de um sal�rio m�nimo.
Elaborar um programa que receba como entrada o valor do sal�rio m�nimo e o valor do sal�rio bruto, calcule o
INSS e o sal�rio l�quido restante e informe-os.*/

#include <stdio.h>
#include <stdlib.h>

int main (void)
{
    float sm, sb, sl, cont;

    printf ("Insira o valor do salario minimo atual: ");
    scanf ("%f", &sm);
    printf ("Insira o valor do salario bruto: ");
    scanf ("%f", &sb);


    if (sb <= (3*sm))
    {
        cont = sb*0.08;

        if (cont >= sm)
        {
            sl = sb - sm;
            printf ("Voce contribui R$%.2f com o INSS e seu salario liquido eh: R$%.2f\n", sm, sl);
        }
        else if (cont < sm)
        {
            sl = sb - (sb*0.08);
            printf ("Voce contribui R$%.2f com o INSS e seu salario liquido eh: R$%.2f\n", 0.08*sb, sl);
        }
    }
    else if (sb > (3*sm))
    {
        cont = sb*0.1;

        if (cont >= sm)
        {
            sl = sb - sm;
            printf ("Voce contribui R$%.2f com o INSS e seu salario liquido eh: R$%.2f\n", sm, sl);
        }
        else if (cont < sm)
        {
            sl = sb - (sb*0.1);
            printf ("Voce contribui R$%.2f com o INSS e seu salario liquido fica: R$%.2f\n", sb*0.1, sl);
        }
    }
    return 0;
}
