/*3) A figura a seguir representa uma �rvore de decis�o para identificar se um paciente est� saud�vel ou
doente. Elabore um programa que implementa essa �rvore de decis�o.*/

#include <stdio.h>
#include <stdlib.h>

int main (void)
{
    int temp;
    char pac;

    printf ("O paciente se sente bem?(S/s/N/n)\n");
    scanf ("%c", &pac);
    setbuf(stdin, NULL);

    if (pac == 's' || pac == 'S')
    {
        printf ("Paciente esta saudavel.\n");
    }
    else if (pac == 'n' || pac == 'N')
    {
        printf ("O paciente tem dor?\n");
        scanf ("%c", &pac);

        if (pac == 'n' || pac == 'N')
        {
            printf ("Informe a temperatura do paciente:\n");
            scanf ("%d", &temp);

            if (temp > 37)
            {
                printf ("Paciente esta doente\n");
            }
            else if (temp <= 37)
            {
                printf ("Paciente esta saudavel\n");
            }
        }
        else
        {
            printf ("Paciente esta doente.\n");
        }
    }
    return 0;
}
