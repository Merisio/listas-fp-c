//8)Fa�a um algoritmo que calcule e mostre a tabuada de 0 a 10 de um n�mero inteiro digitado pelo usu�rio.

#include <stdio.h>

int main (void)

{
//Declara��o de vari�veis
    int num, num0, num1, num2, num3, num4, num5, num6, num7, num8, num9, num10;

//Entrada de dados
    printf ("Insira um numero:");
    scanf ("%d", &num);

//Processamento
        num0 = num*0;
        num1 = num*1;
        num2 = num*2;
        num3 = num*3;
        num4 = num*4;
        num5 = num*5;
        num6 = num*6;
        num7 = num*7;
        num8 = num*8;
        num9 = num*9;
        num10 = num*10;

//Sa�da de dados
    printf ("\nTabuada:");
    printf ("\n%d", num0);
    printf ("\n%d", num1);
    printf ("\n%d", num2);
    printf ("\n%d", num3);
    printf ("\n%d", num4);
    printf ("\n%d", num5);
    printf ("\n%d", num6);
    printf ("\n%d", num7);
    printf ("\n%d", num8);
    printf ("\n%d", num9);
    printf ("\n%\n", num10);

    return 0;
}
