/*2) Escreva um algoritmo que leia o raio de um círculo e calcule a sua circunferência (C = 2. π. r).*/

#include <stdio.h>

float main (void)

{
//Declaração de variáveis
    float  raio, circunferencia;

//Entrada de dados
    printf ("Insira o raio:");
    scanf ("%f", &raio);

//Processamento de dados
    circunferencia = (raio*2*3.1415);

//Saída de dados
    printf ("\nCircunferencia = %.3f\n", circunferencia);

    return 0;
}
