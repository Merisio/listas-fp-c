//5) Tendo como entrada dois valores inteiros, elaborar um algoritmo para calcular e mostrar:

#include <stdio.h>

int main (void)

{
//Declara��o de vari�veis
    int v1, v2, soma, sub12, prod, div12, resto;
    float div12f;

//Entrada de dados
    printf ("Insira um valor:");
    scanf ("%d", &v1);
    printf ("Insira outro valor:");
    scanf ("%d", &v2);

//Processamento
    soma = v1+v2;
    sub12 = v1-v2;
    prod = v1*v2;
    div12 = v1/v2;
    div12f = ((float)v1)/v2;
    resto = v1%v2;

//Sa�da de dados
    printf ("\nSoma= %d", soma);
    printf ("\nSubtracao= %d", sub12);
    printf ("\nProduto= %d", prod);
    printf ("\nDivisao exata= %d", div12);
    printf ("\nDivisao em fracao= %.3f", div12f);
    printf ("\nResto= %d\n", resto);

    return 0;
}
