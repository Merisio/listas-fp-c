//10) Resolva as seguintes express�es matem�ticas. X e Y s�o valores fornecidos pelo usu�rio. Calcule e mostre o resultado de cada express�o. Reutilize vari�veis, ou seja, ter� apenas uma vari�vel para armazenar os resultados do tipo int e outra para armazenar o resultado do tipo float. Faz a primeira opera��o e j� imprime o resultado e assim para todas as demais opera��es. Aten��o para os resultados que podem ser valores float e para a prioridade dos operadores. Primeiro deve-se linearizar as express�es e depois implementar o algoritmo para calcular os resultados.

#include <stdio.h>

int main (void)
{
//Declara��o de vari�veis
    int x, y, resInt;
    float res;

//Entrada de dados
    printf ("Informe o valor de x:");
    scanf ("%d", &x);
    printf ("Informe o valor de y:");
    scanf ("%d", &y);

//Processamento
    res = (((float)x+y)/y)*(x*x);
    printf ("\nValor da expressao a: %.2f", res);

    res = ((float)x+y)/(x-y);
    printf ("\nValor da expressao b: %.2f", res);

    res = ((x*x) + (y*y*y))/2.0;
    printf ("\nValor da expressao c: %.2f", res);

    res = ((float)x*x*x)/(x*x);
    printf ("\nValor da expressao d: %.2f", res);

    resInt = x%y;
    printf ("\nValor da expressao e1: %.2d", resInt);

    resInt = x%3;
    printf ("\nValor da expressao e2: %.2d", resInt);

    resInt = y%5;
    printf ("\nValor da expressao e3: %.2d\n", resInt);

    return 0;
}
