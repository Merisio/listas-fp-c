//6) Calcular o valor do sal�rio l�quido de uma pessoa. Sobre o sal�rio bruto incidem descontos de imposto de renda e de INSS.

#include <stdio.h>

int main (void)
{
//Declara��o de vari�veis
    float sb, ir, inss, sl;

//Entrada de dados
    printf ("Informe o salario bruto:");
    scanf ("%f", &sb);
    printf ("Informe o percentual de INSS:");
    scanf ("%f", &inss);
    printf ("Informe o percentual de IR:");
    scanf ("%f", &ir);

//Processamento de dados
    sl =  sb - ((sb*ir)/100.0) - ((sb*ir)/100.0);

//Sa�da de dados
    printf ("Salario liquido = %.2f ", sl);

    return 0;
}
