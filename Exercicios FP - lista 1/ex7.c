//7) Fa�a um algoritmo que receba uma temperatura em grau Celsius, e calcule e mostre essa temperatura em grau Fahrenheit.

#include <stdio.h>

int main (void)
{
//Declara��o de vari�veis
    float c, f;

//Entrada de dados
    printf ("Insira a temperatura em Celsius:");
    scanf ("%f", &c);

//Processamento
    f = ((1.8*c) + 32);

//Sa�da de dados
    printf ("Temperatura em Fahrenheit: %.1f", f);

return 0;
}
