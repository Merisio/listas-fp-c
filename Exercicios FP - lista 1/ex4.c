//4) Um professor atribui pesos de 1 a 3 para as notas de tr�s avalia��es, respectivamente. Fa�a um algoritmo que receba as notas e calcule e mostre a m�dia ponderada. A m�dia e as notas ser�o valores do tipo float.


#include <stdio.h>

int main (void)

{
//Declara��o de vari�veis
    float  n1, n2, n3, nfinal;

//Entrada de dados
    printf ("Insira a primeira nota:");
    scanf ("%f", &n1);
    printf ("Informe a segunda nota:");
    scanf ("%f", &n2);
    printf ("Informe a terceira nota:");
    scanf ("%f", &n3);

//Processamento de dados
    nfinal = ((1*n1)+(2*n2)+(3*n3))/6;

//Sa�da de dados
    printf ("\nNota Final: %.1f\n", nfinal);

    return 0;
}
