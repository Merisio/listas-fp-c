//12) Calcule e mostre a m�dia de idade de tr�s pessoas.

#include <stdio.h>

int main (void)
{
//Declara��o de vari�veis
    int i1, i2, i3, media;

//Entrada de dados
    printf ("Insira a idade da primeira pessoa:");
    scanf ("%d", &i1);
    printf ("Insira a idade da segunda pessoa:");
    scanf ("%d", &i2);
    printf ("Insira a idade da terceira pessoa:");
    scanf ("%d", &i3);

//Processamento
    media = (i1 + i2 + i3)/3;

//Sa�da de dados
    printf ("\nIdade media: %d anos\n", media);

    return 0;
}
