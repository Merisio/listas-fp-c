//11) O que faz o algoritmo a seguir representado em fluxograma? Implemente-o utilizando a linguagem C.

#include <stdio.h>

int main (void)
{
//Declara��o de vari�veis
    float salAt, percReaj, salReaj;

//Entrada de dados
    printf ("Informe o salario atual: ");
    scanf ("%f", &salAt);
    printf ("Informe o percentual de reajuste (0 a 100%%): ");
    scanf ("%f", &percReaj);

//Processamento
    salReaj = ((salAt*percReaj)/100.0) + salAt;

//Sa�da de dados
    printf ("\nSalario reajustado: R$%.2f\n", salReaj);

    return 0;
}
