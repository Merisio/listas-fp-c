/*3) Escreva um algoritmo que leia o valor de uma presta��o e da taxa de juros cobrada pelo atraso da presta��o. Calcule o valor a ser pago por meio da f�rmula:*/

#include <stdio.h>

int main (void)

{
//Declara��o de vari�veis
    float  valorComJuros, taxa, prestacao;\n

//Entrada de dados
    printf ("Insira a taxa:");
    scanf ("%f", &taxa);
    printf ("Insira a prestacao:");
    scanf ("%f", &prestacao);

//Processamento de dados
    valorComJuros = prestacao + (prestacao*taxa/100);

//Sa�da de dados
    printf ("\nValor final a pagar: %.2f\n", valorComJuros);

    return 0;
}
