/*1) Escreva um algoritmo que leia o comprimento (cm), a largura (cm) e a altura (cm) de uma caixa retangular e calcule o seu volume (cm3), cuja f�rmula �:
Volume = Comprimento * Largura * Altura*/

#include <stdio.h>

float main (void)

{
//Declara��o de vari�veis
    float  comprimento, largura, altura, volume;

//Entrada de dados
    printf ("Insira o comprimento:");
    scanf ("%f", &comprimento);
    printf ("Informe a largura:");
    scanf ("%f", &largura);
    printf ("Informe a altura:");
    scanf ("%f", &altura);

//Processamento de dados
    volume =(comprimento*altura*largura);

//Sa�da de dados
    printf ("\nValor do volume: %.2f\n", volume);

    return 0;
}
