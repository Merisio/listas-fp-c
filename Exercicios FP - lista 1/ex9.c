//9) Leia dois valores A e B e efetue a troca do conte�do das duas vari�veis lidas, de forma que a vari�vel A passe a ter o valor de B e a vari�vel B o valor de A. Mostre o conte�do das vari�veis antes e depois da troca.


#include <stdio.h>

int main (void)
{
//Declara��o de vari�veis
    int a, b, aux;

//Entrada de dados
    printf ("Informe o valor de a:");
    scanf ("%d", &a);
    printf ("Informe o valor de b:");
    scanf ("%d", &b);
    printf ("\nAntes da troca, a= %d\n", a);
    printf ("Antes da troca, b= %d\n", b);

//Processamento
    aux = a;
    a = b;
    b = aux;

//Sa�da de dados
    printf ("\nDepois da troca, a= %d\n", a);
    printf ("Depois da troca, b= %d\n", b);

    return 0;
}



