/*2) Fazer um programa para ler o sal�rio de uma pessoa, o percentual de aumento e o percentual de descontos.
Os descontos incidem sobre o sal�rio com aumento. Calcular o novo sal�rio e mostr�-lo como no exemplo a
seguir.*/

#include <stdio.h>

int main(void)
{
    float sal, aum, des, salAumt, liq;

    printf ("Insira o valor do salario em reais:");
    scanf ("%f", &sal);
    printf ("Insira o percentual de aumento (%%):");
    scanf ("%f", &aum);
    printf ("Insira o percentual de desconto (%%):");
    scanf ("%f", &des);

    salAumt = sal + (sal*aum)/100;
    liq = salAumt - (salAumt*des)/100;

    printf ("\nO salario com aumento eh: R$%.2f\n", salAumt);
    printf ("O salario liquido eh: R$%.2f\n", liq);

    return 0;
}
