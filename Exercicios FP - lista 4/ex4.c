/*4) Ler um n�mero inteiro com at� 5 d�gitos. Separar os d�gitos desse n�mero e mostr�-los em linhas distintas.
Tamb�m calcular e mostrar a soma dos d�gitos.*/

#include <stdio.h>

int main(void)
{
    int num, a, b, c, d , e, soma;

    printf ("Informe um numero com 5 algarismos ou menos:");
    scanf ("%d", &num);

    a = num/10000;
    b = (num%10000)/1000;
    c = ((num%10000)%1000)/100;
    d = (((num%10000)%1000)%100)/10;
    e = (((num%10000)%1000)%100)%10;
    soma = a+b+c+d+e;

    printf ("\nO primeiro algarismo eh: %.d\n", a);
    printf ("O segundo algarismo eh: %.d\n", b);
    printf ("O terceiro algarismo eh: %.d\n", c);
    printf ("O quarto algarismo eh: %.d\n", d);
    printf ("O quinto algarismo eh: %.d\n", e);
    printf ("A soma dos algarismos eh: %.d\n", soma);

    return 0;
}
