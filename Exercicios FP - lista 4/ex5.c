/*1) Ler um n�mero double. Separar a parte inteira e a parte decimal desse n�mero. Apresentar a parte decimal
como um valor double e como um inteiro de tr�s d�gitos. Da parte inteira separar o n�mero que representa
unidade, dezena e centena e mostrar.*/

#include <stdio.h>

int main(void)
{
    int num, a, b, c, inv;

    printf("Informe um numero com 3 algarismos ou menos");
    scanf("%d",&num);

    a = num/100;
    b = (num%100)/10;
    c = (num%100)%10;
    inv = (c*100)+(b*10)+a;

    printf("\nO primeiro algarismo eh: %d\n", a);
    printf("O segundo algarismo eh: %d\n", b);
    printf("O terceiro algarismo eh: %d\n", c);
    printf("O inverso do numero eh: %d\n", inv);

    return 0;
}
