/*3) Ler um n�mero inteiro com at� 4 d�gitos. Separar os d�gitos desse n�mero e mostr�-los em linhas distintas.*/

#include <stdio.h>

int main(void)
{
    int num, a, b, c, d;

    printf ("Insira um numero de 4 digitos ou menos:");
    scanf ("%d", &num);

    a = num/1000;
    b = (num%1000)/100;
    c = ((num%1000)%100)/10;
    d = ((num%1000)%100)%10;

    printf ("\nO Primeiro digito eh: %.d\n", a);
    printf ("O Segundo digito eh: %.d\n", b);
    printf ("O Terceiro digito eh: %.d\n", c);
    printf ("O Quarto digito eh: %.d\n", d);

    return 0;
}
