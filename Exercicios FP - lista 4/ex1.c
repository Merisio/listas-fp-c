/*1) Fazer um programa que leia um valor double que representa o sal�rio de uma pessoa. Apresente
separadamente os reais (parte inteira) e os centavos (parte decimal)*/

#include <stdio.h>

int main(void)
{
    int  reais;
    float sal, cent;

    printf("Informe o valor do salario:");
    scanf("%f", &sal);

    reais = ((int)sal);
    cent = sal-reais;
    cent = cent*100;

    printf ("\nO salario eh: R$%.2f\n", sal);
    printf ("Parte em reais: %d\n", reais);
    printf ("Parte em centavos: %.0f\n", cent);

    return 0;
}
