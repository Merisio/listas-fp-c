/*9) Escreva um programa que o leia o n�mero de horas trabalhadas por um funcion�rio, o valor por hora, o
n�mero de filhos com idade menor do que 14 anos, o valor do sal�rio fam�lia por filho e calcule e mostre o
sal�rio desse funcion�rio.*/

#include <stdio.h>

int main (void)
{
int numf, numh;
float valh, valsf, sal;

printf ("Informe o numero de horas trabalhadas:");
scanf ("%d", &numh);
printf ("Informe o valor da hora trabalhada:");
scanf ("%f", &valh);
printf ("Informe o numero de filhos menores que 14 anos:");
scanf ("%d", &numf);
printf ("Informe o valor do salario familia:");
scanf ("%f", &valsf);

sal = (numh*valh) + (numf*valsf);

printf ("\nSalario: R$%.2f\n", sal);

return 0;
}
