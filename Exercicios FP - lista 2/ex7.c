/*7) Um viajante de carro far� o trajeto entre duas cidades e ao t�rmino da viagem deseja saber:*/

#include <stdio.h>
#include <math.h>

int main (void)
{
float dis, cap, cmed, quantl, quantv, rest;

printf ("Informe a distancia percorrida entre as cidades (em km):");
scanf ("%f", &dis);
printf ("Informe a capacidade maxima do tanque:");
scanf ("%f", &cap);
printf ("Informe o consumo medio (em km por litro):");
scanf ("%f", &cmed);

quantl = dis/cmed;
quantv = ceil (dis/(cmed*cap));
rest = (quantv*cap) - (dis/cmed);


printf ("\nForam necessarios %.1f litros de combustivel para percorrer o trajeto.\n", quantl);
printf ("\nFoi necessario abastecer o veiculo %.0f vezes.\n", quantv);
printf ("\nRestou no tanque de combustivel %.0f litros.\n", rest);

return 0;
}
