/*2) O custo ao consumidor de um carro novo � a soma do custo de f�brica com a percentagem do distribuidor
e a percentagem dos impostos (ambas aplicadas sobre o custo de f�brica). Escrever um programa para, a
partir do custo de f�brica do carro, calcular e mostrar o custo ao consumidor.*/


#include <stdio.h>

int main (void)
{
float cusf, pd, pi, cusc;

printf ("Informe o custo de fabrica do veiculo:");
scanf ("%f", &cusf);
printf ("Insira a percentagem do distribuidor:");
scanf ("%f", &pd);
printf ("Insira a percentagem dos impostos:");
scanf ("%f", &pi);

cusc = cusf+((pd/100)*cusf)+((pi/100)*cusf);

printf ("\nO custo para o consumidor eh: %.2f\n", cusc);

return 0;
}
