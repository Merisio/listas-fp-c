/*5) Escreva um programa para ler o n�mero de votos brancos, nulos (incluem eleitores ausentes) e v�lidos de
uma elei��o. Calcular e mostrar o percentual que cada um (brancos, nulos e v�lidos) representa em rela��o
ao total de eleitores. Lembrar que os valores dos percentuais podem n�o ser inteiros.*/

#include <stdio.h>

int main (void)
{
int vv, vb, vn, vt;
float pvv, pvb, pvn;

printf ("Informe o numero de votos validos:");
scanf ("%d", &vv);
printf ("Informe o numero de votos brancos:");
scanf ("%d", &vb);
printf ("Informe o numero de votos nulos:");
scanf ("%d", &vn);

vt = vv+vb+vn;
pvv = ((float)vv/vt)*100;
pvb = ((float)vb/vt)*100;
pvn = ((float)vn/vt)*100;

printf ("\nVotos totais: %.0d", vt);
printf ("\nPorcentagem de votos validos: %.1f%%", pvv);
printf ("\nPorcentagem de votos brancos: %.1f%%", pvb);
printf ("\nPorcentagem de votos nulos: %.1f%%\n", pvn);

return 0;
}
