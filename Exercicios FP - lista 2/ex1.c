/*1) Ler dois n�meros float e apresentar, sem utilizar fun��es matem�ticas prontas:
a) A divis�o do primeiro n�mero pelo segundo, armazenando somente a parte inteira do n�mero.
b) A soma dos dois n�meros com o resultado arredondado para o pr�ximo n�mero inteiro.*/

#include <stdio.h>
#include <math.h>

int main (void)
{
float x, y;
int resa, resb;

printf ("Insira um numero float:");
scanf ("%f", &x);
printf ("Insira outro numero float:");
scanf ("%f", &y);

resa = x/y;
resb = round(x+y);

printf ("\nO resultado de A eh: %.0d", resa);
printf ("\nO resultado de B eh: %.0d\n", resb);

return 0;
}
