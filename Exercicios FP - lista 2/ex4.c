/*4) Escreva um programa que, dados a quantidade de litros de combust�vel utilizada, os quil�metros
percorridos por um autom�vel e o valor do litro de combust�vel, calcule quantos quil�metros o ve�culo
percorreu por litro de combust�vel e o valor gasto em reais por km.*/

#include <stdio.h>

int main (void)
{
float km, cc, vl, aut, gr;

printf ("Informe os quilometros percorridos:");
scanf ("%f", &km);
printf ("Informe o combustivel consumido (em litros):");
scanf ("%f", &cc);
printf ("Informe o valor do litro de combustivel:");
scanf ("%f", &vl);

aut = km/cc;
gr = (vl*cc)/km;

printf ("\nO automovel fez %.2fkm por litro de combustivel.\n", aut);
printf ("\nO gasto em reais por km foi de R$%.2f\n", gr);

return 0;
}
