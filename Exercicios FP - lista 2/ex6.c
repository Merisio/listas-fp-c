/*6) Fa�a um programa que leia o pre�o de uma mercadoria com diferen�a de um m�s (ler o valor da
mercadoria no m�s passado e no m�s atual) e calcule a taxa de infla��o mensal dessa mercadoria. A infla��o
� o percentual da diferen�a de pre�os (atual menos o anterior) sobre o pre�o anterior.*/

#include <stdio.h>

int main (void)
{
float pmp, pma, tax;

printf ("Informe o preco da mercadoria no mes passado:");
scanf ("%f", &pmp);
printf ("Informe o preco da mercadoria no mes atual:");
scanf ("%f", &pma);

tax = ((pma-pmp)/pmp)*100;

printf ("\nA taxa de inflacao eh: %.1f%%\n", tax);

return 0;
}
