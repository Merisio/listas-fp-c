/*8) Fa�a um programa que leia o sal�rio bruto mensal de um funcion�rio, calcule e mostre os valores conforme
o exemplo a seguir. Observa��o: � poss�vel fazer esse programa utilizando somente tr�s vari�veis: uma para
ler o sal�rio bruto, outra para os descontos e outra para o sal�rio l�quido.*/

#include <stdio.h>
#include <math.h>

int main (void)
{
float sl, sb, ir, inss, sind;

printf ("Informe o salario bruto:");
scanf ("%f", &sb);
printf ("Informe o IR:");
scanf ("%f", &ir);
printf ("Informe o INSS:");
scanf ("%f", &inss);
printf ("Informe o sindicato:");
scanf ("%f", &sind);

sl = sb - ((ir*sb)/100) - ((inss*sb)/100) - ((sind*sb)/100);

printf ("\nSalario liquido: R$%.2f\n", sl);

return 0;
}
