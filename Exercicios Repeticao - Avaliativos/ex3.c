/* 3) Completar e corrigir o c�digo a seguir para:
a) Ler a quantidade somente se a categoria � v�lida.
b) Garantir que a quantidade seja maior que 0.
c) Validar para que n�o seja realizada uma divis�o por zero no c�lculo da m�dia.
d) Permitir a leitura da categoria nas execu��es sucessivas do programa.
e) Garantir que a m�dia seja float */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main (void)
{
    char categ;
    int qua, soma=0, total=0;
    float media;

    do
    {
        printf ("Informe a categoria: ");
        setbuf (stdin,NULL);
        scanf ("%c", &categ);
        categ = toupper(categ);

        if(categ == 'A' || categ == 'B')
        {
            do
            {
                printf ("Informe a quantidade: ");
                scanf ("%d", &qua);
                total++;
            }
            while (qua<=0);

            soma = soma + qua;
            media = ((float)soma/total);
        }
    }
    while (categ == 'A' || categ == 'B');

    printf ("\nA media dos produtos eh %.2f\n", media);

    return 0;
}
