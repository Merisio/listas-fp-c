/* 2) Uma empresa deseja calcular a depreciação de seus bens. Para tanto, desenvolver um programa que
obtenha a taxa de depreciação anual para os bens, o valor do bem a ser depreciado e o período em anos.
Valor depreciado = valor do bem * (taxa de depreciação / 100)
Valor do bem depreciado = valor do bem – valor depreciado */

#include <stdio.h>
#include <stdlib.h>

int main (void)
{
    float td, vb, vd, da;
    int ano, cont=0, x;

    printf ("Informe o valor do bem a ser depreciado : R$");
    scanf ("%f", &vb);
    printf ("Informe o periodo da depreciacao (em anos): ");
    scanf ("%d", &ano);
    printf ("Informe a taxa de depreciacao(em %%): ");
    scanf ("%f", &td);

    printf ("Ano \t Valor do Bem \t Depreciacao \t Valor Depreciado\n");
    printf ("=========================================================\n");

    for (x=0; x<ano; x++)
    {
        vd = vb*(td/100);
        cont++;

        printf ("%d \t %7.2f \t %7.2f \t %7.2f\n", cont, vb, vd, vb-vd);

        da = da+vd;
        vb = vb-vd;
        vd = vb*(td/100);
    }
    printf ("=========================================================\n");
    printf ("Depreciacao acumulada: R$ %.2f\n", da);

    return 0;
}
