/*Apresentar os n�meros primos entre dois valores, que representam os limites inferior e superior,
respectivamente, de um intervalo, informados pelo usu�rio. Apresent�-los com n n�meros por linha. n �
informado pelo usu�rio. Validar n para que seja maior que 0. Validar o limite inferior para seja maior que 1 e o
limite superior para que seja maior ou igual ao limite inferior. Implementar a repeti��o de programa.
Exemplo de entrada e sa�da:*/

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

int main (void)
{
    int num, aux, ls, li, n, i;
    char opcao;

    do
    {
        printf("Informe o valor do limite inferior do intervalo: ");
        scanf("%d", &li);
        printf("Informe o valor do limite superior do intervalo: ");
        scanf("%d", &ls);
        printf("Informe quantos numeros deseja imprimir por linha: ");
        scanf("%d", &n);

        aux = n;

        if(n>0 && li>1 && ls>=li)
        {
            for(num=li; num<=ls; num++)
            {
                int cont = 0;
                for (i=1; i<=(ls); i++)
                {
                    if (num%i == 0)
                    {
                        cont++;
                    }
                }
                 if (cont == 2)
                {
                    if (n>0)
                    {
                        printf("%10.d", num);
                        n--;
                    }
                    else
                    {
                        printf ("\n");
                        printf ("%10.d", num);
                        n = (aux-1);
                    }
                }
            }
        }
        if (n<=0)
        {
            printf ("\nNumero de linhas invalido.");
        }
        if(li<1)
        {
            printf ("\nLimite inferior invalido.");
        }
        if(ls<li)
        {
            printf ("\nLimite superior iinvalido.");
        }

        printf ("\n\nDeseja repetir o programa? <S/s ou N/n>\n");
        setbuf(stdin, NULL);
        scanf ("%c", &opcao);
    }
    while(opcao == 'S' || opcao == 's');

    return 0;
}
