/*Ler um n�mero e ler um d�gito. Contar quantos d�gitos o n�mero possui*/

#include<stdlib.h>
#include <stdio.h>
#include <ctype.h>

int main (void)
{
    char opcao;
    int num, dig, aux;

    do
    {
        int qtd=0;

        printf("Digite um numero: ");
        scanf("%d", &num);
        printf("Digite um digito: ");
        scanf("%d", &dig);

        if(num>=0 && dig<10 && dig>=0)
        {
            while(num>0)
            {
                aux = num%10;
                num = (num/10);
                if(aux == dig)
                {
                    qtd++;
                }
            }
            printf("\nO numero tem %d digito(s) igual(s) a %d.\n", qtd, dig);
        }
        else
        {
            printf("\nDigito invalido. O valor deve ser um inteiro entre 0 e 9.\n");
        }

        printf("\nDeseja repetir o programa? <S/s ou N/n>\n");
        setbuf(stdin, NULL);
        scanf("%c", &opcao);

    }
    while(opcao == 'S' || opcao == 's');

    return 0;
}

