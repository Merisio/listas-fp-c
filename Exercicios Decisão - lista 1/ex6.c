/*6) Elaborar um programa que leia um valor que se refere � altura de uma pessoa e mostre uma mensagem
conforme a tabela a seguir. Utilizar vari�vel do tipo float.*/

#include <stdio.h>
#include <stdlib.h>

int main (void)
{
    float alt;

    printf ("Insira a sua altura:");
    scanf ("%f", &alt);

    if (alt<1.50)
        printf ("Altura menor que 1,5 metros.");

    else
    {
        if (alt>=1.50 && alt<=1.80)
            printf ("Altura entre 1,5 e 1,8 metros.");

        else
            printf ("Altura maior que 1,8 metros.");
    }
    return 0;
}
