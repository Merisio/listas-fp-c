/*3) Elaborar um programa que leia um caractere. Se o caractere informado for �F� ou �f� mostrar a mensagem
�pessoa f�sica�, se informado �J� ou �j� mostrar a mensagem �pessoa jur�dica� ou mostrar "caractere inv�lido"
para qualquer outro caractere.*/

#include <stdio.h>
#include <stdlib.h>

int main (void)
{
    char let;

    printf ("Insira uma letra:");
    scanf ("%c", &let);

    if (let == 'F' || let == 'f')
        printf ("\nPessoa fisica.\n");

    else
    {
        if (let == 'J' || let == 'j')
        printf ("\nPessoa juridica.\n");

        else
        printf ("\nCaractere invalido.\n");
    }

    return 0;
}
