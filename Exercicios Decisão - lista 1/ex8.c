/*8) Fa�a um programa que leia tr�s notas de um aluno e calcule a m�dia ponderada, com os pesos 1, 3 e 4,
respectivamente, e:*/

#include <stdio.h>
#include <stdlib.h>

int main (void)
{
    float n1, n2, n3, med, rec, nf;

    printf ("Insira a primeira nota:");
    scanf ("%f", &n1);
    printf ("Insira a segunda nota:");
    scanf ("%f", &n2);
    printf ("Insira a terceira nota:");
    scanf ("%f", &n3);

    med = ((n1*1)+(n2*3)+(n3*4))/8;
    printf ("Media = %.1f", med);

    if (med<=10 && med>=6)
        printf ("\nO aluno esta aprovado\n");

    else
    {
        if (med>=4 && med<6)
            {
                printf ("\nO aluno esta em recuperacao.");
                printf ("\nInsira a nota de recuperacao:");
                scanf ("%f", &rec);

                nf = (med + rec)/2;

                if (nf>=5)
                    printf ("\nO aluno esta aprovado com nota %.1f\n", nf);

                else
                    printf ("\nO aluno esta reprovado com nota %.1f\n", nf);
            }
        else
            if (med<4)
                printf ("\nO aluno esta reprovado.\n");
    }
    return 0;
}
