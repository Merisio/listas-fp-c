/*2) Escreva um programa que leia um n�mero e verifique se ele � maior, menor ou igual a 10.*/

#include <stdio.h>

int main (void)
{
    int num;

    printf ("Insira um numero inteiro:");
    scanf ("%d", &num);

    if (num == 10)
        printf ("\nO numero eh igual a 10.\n");

    else
    {
        if (num < 10)
        printf ("\nO numero eh menor que 10.\n");

        if (num > 10)
        printf ("\nO numero eh maior que 10.\n");
    }

    return 0;
}
