//1) Fa�a um programa que receba o custo de um espet�culo teatral e o pre�o do convite desse espet�culo.

#include <stdio.h>
#include <math.h>

int main (void)
{
//Declara��o de vari�veis
    float ce, vc;
    int qcv, qcvl;

//Entrada de dados
    printf ("Insira o valor de custo do espetaculo:");
    scanf ("%f", &ce);
    printf ("Insira o valor do convite:");
    scanf ("%f", &vc);

//Processamento
    qcv = ceil (ce/vc);
    qcvl = ceil ((ce*1.25)/vc);

//Sa�da de dados
    printf("\nQuantidade de ingressos necessaria para cobrir o valor: %.0d\n", qcv);
    printf("Quantidade de ingressos necessaria para obter lucro de 25%%: %.0d\n ", qcvl);

    return 0;
}
