//3) Cada degrau de uma escada tem uma altura X. Fa�a um programa para ler essa altura e a altura que o usu�rio deseja alcan�ar subindo a escada. Calcule e mostre quantos degraus o usu�rio dever� subir para atingir o seu objetivo.

#include <stdio.h>
#include <math.h>

int main (void)
{
//Declara��o de vari�veis
    float altd, altf, numd;

//Entrada de dados
    printf ("Insira a altura do degrau em metros:");
    scanf ("%f", &altd);
    printf ("Insira a altura que se deseja chegar em metros:");
    scanf ("%f", &altf);

//Processamento
    numd = ceil (altf/altd);

//Sa�da de dados
    printf ("\nQuantidade de degraus a subir: %.0f\n ", numd);

    return 0;
}
