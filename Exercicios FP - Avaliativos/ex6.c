/*6) Uma pessoa resolveu fazer uma aplica��o em uma poupan�a programada. Para calcular seu rendimento,
ela dever� fornecer o valor constante da aplica��o mensal, a taxa de juros (%) e o prazo da aplica��o (em
meses).*/

#include <stdio.h>
#include <math.h>

int main (void)
{
//Declara��o de vari�veis
    float taxa, vm, nm, va ;

//Entrada de dados
    printf ("Insira o prazo em meses:");
    scanf ("%f", &nm);
    printf ("Insira a taxa (entre 0 e 1):");
    scanf ("%f", &taxa);
    printf ("Insira o valor de aplicacao mensal:");
    scanf ("%f", &vm);

//Processamento
    va = (vm*(pow ((1+taxa),nm) - 1))/taxa;

//Sa�da de dados
    printf ("\nO valor acumulado eh: %.2f\n", va);


    return 0;
}

