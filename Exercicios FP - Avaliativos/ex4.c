/*4) Fa�a um programa que receba o peso e a altura de uma pessoa e calcule o �ndice de massa corp�rea.
Esse �ndice mede a rela��o entre peso e altura (peso em quilogramas dividido pelo quadrado da altura em
metros). Use a fun��o pow() da biblioteca math.h para calcular a pot�ncia.*/

#include <stdio.h>
#include <math.h>

int main (void)
{
//Declaral�ao de vari�veis
    float imc, alt, peso;

//Entrada de dados
    printf ("Insira a altura:");
    scanf ("%f", &alt);
    printf ("Insira o peso:");
    scanf ("%f", &peso);

//Processamento
    imc = peso/(pow(alt,2));

//Sa�da de dados
    printf ("\nO IMC eh: %.2f\n", imc);

    return 0;
}
