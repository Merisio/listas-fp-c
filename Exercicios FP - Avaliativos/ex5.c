/*5) Um hotel deseja fazer uma promo��o especial de final de semana, concedendo um desconto de 25% na
di�ria. Sendo informados o n�mero de apartamentos do hotel e o valor da di�ria por apartamento para o final
de semana completo. Elaborar um programa para calcular:*/

#include <stdio.h>

int main (void)
{
//Declara��o de vari�veis
    int quantAp;
    float valAp, valpro, tot100, tot70, valPerd;

//Entrada de dados
    printf ("Insira o valor da diaria:");
    scanf ("%f", &valAp);
    printf ("Insira o numero de apartamentos:");
    scanf ("%d", &quantAp);

//Processamento
    valpro = 0.75*valAp;
    tot100 = quantAp*valpro;
    tot70  = (quantAp*0.7)*valpro;
    valPerd = 0.25*(quantAp*valAp);

//Sa�da de dados
    printf ("\nValor da diaria promocional: %.2f", valpro);
    printf ("\nTotal arrecadado com 100%: %.2f", tot100);
    printf ("\nTotal arrecadado com 70%: %.2f", tot70);
    printf ("\nValor perdido com descontos: %.2f\n", valPerd);

    return 0;
}
