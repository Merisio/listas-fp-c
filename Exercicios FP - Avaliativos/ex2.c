//2) Elabore um programa que leia via teclado uma quantidade de segundos (tipo int) e transforme este tempo em dias, horas e minutos (as tr�s �ltimas em tipo float).

#include <stdio.h>

int main (void)
{
//Declara��o de vari�veis
    int seg;
    float hor, min, dias;

//Entrada de dados
    printf ("Insira os segundos:");
    scanf ("%d", &seg);

//Processamento
    hor = seg/3600.0;
    min = seg/60.0;
    dias = seg/86400.0;

//Sa�da de dados
    printf ("\nHoras: %.0f", hor);
    printf ("\nMinutos: %.0f", min);
    printf ("\nDias: %.0f\n", dias);

    return 0;
}
