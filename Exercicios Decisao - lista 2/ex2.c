/*2) Ler tr�s valores inteiros diferentes, encontrar e mostrar o n�mero do meio. Deve ser criada uma vari�vel
para armazenar o n�mero do meio.*/

#include <stdio.h>
#include <stdlib.h>

int main (void)
{
    int num1, num2, num3, meio;

    printf ("Insira um numero:");
    scanf ("%d", &num1);
    printf ("Insira mais um numero:");
    scanf ("%d", &num2);
    printf ("Insira mais um numero:");
    scanf ("%d", &num3);

    if (num1<num3 && num1>num2 || num1<num2 && num1>num3)
        {
            meio = num1;
            printf ("\nO numero do meio eh %d\n", meio);
        }
    else
    {
        if (num2>num1 && num2<num3 || num2<num1 && num2>num3)
            {
                meio = num2;
                printf ("\nO numero do meio eh %d\n", meio);
            }

        else
            {
                 meio = num3;
                 printf ("\nO numero do meio eh %d\n", meio);
            }
    }
    return 0;
}
