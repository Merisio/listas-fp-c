/*8) Fazer o programa para o algoritmo representado no fluxograma a seguir:*/

#include <stdio.h>
#include <stdlib.h>

int main (void)
{
    float sb, sbr, grat, ir;

    printf ("Insira o salario base:");
    scanf ("%f", &sb);
    printf ("Insira a gratificacao:");
    scanf ("%f", &grat);

    sbr = sb+grat;

    if (sbr<1000)
    {
        ir = sbr*(0.15);
        printf ("O salario liquido eh: R$%.2f", sbr-ir);
    }
    else
    {
        ir = sbr*(0.2);
        printf ("O salario liquido eh: R$%.2f", sbr-ir);
    }
return 0;
}
