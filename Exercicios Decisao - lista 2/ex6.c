/*6) Ler tr�s valores inteiros que representam os lados de um tri�ngulo e determinar se esses valores podem
formar um tri�ngulo (obs.: para ser um tri�ngulo cada lado deve ser menor que a soma dos outros dois lados).
Se for um tri�ngulo, determinar o seu tipo: equil�tero (todos os lados iguais), is�sceles (dois lados iguais) e
escaleno (todos os lados diferentes).*/

#include <stdio.h>
#include <stdlib.h>

int main (void)
{
    int l1, l2, l3;

    printf ("Insira um valor inteiro:");
    scanf ("%d", &l1);
    printf ("Insira um valor inteiro:");
    scanf ("%d", &l2);
    printf ("Insira um valor inteiro:");
    scanf ("%d", &l3);

    if (l1<l2+l3 && l2<l1+l3 && l3<l1+l3)
    {
        if (l1==l2 && l2==l3)
        {
            printf ("\nOs valores formam um triangulo equilatero.\n");
        }
        else if (l1==l2 || l2==l3 || l1==l3)
        {
            printf ("\nOs valores formam um triangulo isosceles.\n");
        }
        else
        {
            printf ("\nOs valores formam um triangulo escaleno.\n");
        }
    }
    else
        {
        printf ("\nOs valores nao podem formar um triangulo\n");
        }

    return 0;
}
