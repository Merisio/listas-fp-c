/*7) Ler um n�mero e utilizando uma estrutura if else if else if... (obrigatoriamente encadeada) informar se ele:
a) � divis�vel por 5, por 3 ou por 2;
b) � divis�vel somente por 5 e por 3; por 5 e por 2; ou por 3 e por 2;
c) � divis�vel somente por 5, por 3 ou por 2;
d) N�o � divis�vel por nenhum destes;
*/

#include <stdio.h>
#include <stdlib.h>

int main (void)
{
    int num;

    printf ("Insira um valor inteiro:");
    scanf ("%d", &num);

    if (num%2 == 0 && num%3 == 0 && num%5 == 0)
    {
        printf ("\nO numero eh divisivel por 2, 3 e 5.\n");
    }
    else if (num%2 == 0 && num%3 == 0)
        {
            printf ("\nO numero eh divisivel por 2 e 3.\n");
        }
        else if (num%2 == 0 && num%5 == 0)
            {
                printf ("\nO numero eh divisivel por 2 e 5.\n");
            }
            else if  (num%5 == 0 && num%3 == 0)
                {
                    printf ("\nO numero eh divisivel por 3 e 5.\n");
                }
                else if (num%5 == 0)
                    {
                        printf ("\nO numero eh divisivel por 5.\n");
                    }
                    else if (num%2 == 0)
                        {
                            printf ("\nO numero eh divisivel por 2.\n");
                        }
                        else if (num%3 == 0)
                            {
                                printf ("\nO numero eh divisivel por 3.\n");
                            }
                            else
                                printf ("\nO numero nao eh divisivel por 2, 3 ou 5.\n");

    return 0;
}
