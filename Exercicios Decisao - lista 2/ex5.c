/*5) Calcular o imposto de renda de uma pessoa de acordo com a seguinte tabela:*/

#include <stdio.h>
#include <stdlib.h>

int main (void)
{
    float sal, ir;

    printf ("Insira o salario:");
    scanf ("%f", &sal);

    if (sal>=10000)
    {
        if (sal>=10000 && sal<25000)
        {
            ir = 0.1*sal;
            printf ("\nO imposto a ser pago eh de R$%.2f\n", ir);
        }
        else
        {
            if (sal>=25000)
            {
                 ir = 0.25*sal;
                 printf ("\nO imposto a ser pago eh de R$%.2f\n", ir);
            }
        }
    }
    else
    {
        if (sal<10000 && sal>0)
            printf ("\nNao eh necessario pagar imposto de renda.\n");
        else
            printf ("\nImpossivel realizar a operacao.\n");
    }
}
