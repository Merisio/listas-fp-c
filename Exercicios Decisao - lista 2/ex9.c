/*9) N�meros pal�ndromos s�o aqueles que escritos da direita para a esquerda ou da esquerda para a direita
tem o mesmo valor. Ex.: 9229, 4554, 9779. Fazer um programa que dado um n�mero de 4 d�gitos, calcular e
escrever se este n�mero � ou n�o pal�ndromo.*/

#include <stdio.h>
#include <stdlib.h>

int main (void)
{
    int uni, dez, cen, mil, num;

    printf ("Insira um numero de 4 digitos:");
    scanf ("%d", &num);

    mil = num/1000;
    cen = num%1000/100;
    dez = num%100/10;
    uni = num%10;

    if (mil==uni && dez==cen)
    {
        printf ("\nO numero eh palindromo.\n");
    }
    else
    {
        printf ("\nO numero nao eh palindromo.\n");
    }

    return 0;
}
