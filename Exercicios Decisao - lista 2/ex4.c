/*4) Ler o g�nero (F ou f para feminino, M ou m para masculino. Para qualquer outro caractere informar que �
inv�lido e finalizar o programa). Se informado um caractere v�lido, ler a altura da pessoa e calcular e mostrar*/

#include <stdio.h>
#include <stdlib.h>

int main (void)
{
    char gen;
    float alt, pi;

    printf ("Insira o seu sexo (use apenas a primeira letra):");
    scanf ("%c", &gen);

    if (gen == 'M' || gen == 'm' || gen == 'F' || gen == 'f')
    {
        if (gen == 'M' || gen == 'm')
        {
            printf ("Insira a sua altura:");
            scanf ("%f", &alt);
            pi = (72.7 * alt) - 58;
            printf ("\nSeu peso ideal eh: %.1fKg\n", pi);
        }
        else
        {
            if (gen == 'F' || gen == 'f')
                {
                    printf ("Insira a sua altura:");
                    scanf ("%f", &alt);
                    pi = (62.1 * alt) - 44.7;
                    printf ("\nSeu peso ideal eh: %.1fKg\n", pi);
                }
        }
    }
    else
        printf ("\nCaractere invalido.\n");

    return 0;
}
