/*10) Fa�a um programa que solicite ao usu�rio o valor do sal�rio de um funcion�rio e apresente o menu a
seguir e permita ao usu�rio escolher a op��o desejada e mostre o resultado. Verifique a possibilidade de
op��o inv�lida e n�o se preocupe com restri��es, como sal�rio negativo. Use switch - case, if e if - else para a
solu��o.*/

#include <stdio.h>
#include <stdlib.h>

int main (void)
{
    float sal;
    char opcao;

    printf ("Insira o salario:");
    scanf ("%f", &sal);
    setbuf(stdin, NULL);
    printf ("\nMenu de opcoes:\n");
    printf ("A - Imposto\n");
    printf ("B - Novo salario\n");
    printf ("C - Classificacao\n");
    printf ("Selecione a opcao desejada:");
    scanf ("%c", &opcao);

    switch (opcao)
    {
        case 'A':
        {
            if (sal<1000)
            {
                printf ("\nSalario com imposto descontado: R$%.2f\n", sal-(0.05*sal));
            }
            else if (sal>=1000 && sal<1500)
            {
                printf ("\nSalario com imposto descontado: R$%.2f\n", sal-(0.1*sal));
            }
            else
            {
                printf ("\nSalario com imposto descontado: R$%.2f\n", sal-(0.15*sal));
            }
        break;
        }
        case 'a':
        {
            if (sal<1000)
            {
                printf ("\nSalario com imposto descontado: R$%.2f\n", sal-(0.05*sal));
            }
            else if (sal>=1000 && sal<1500)
            {
                printf ("\nSalario com imposto descontado: R$%.2f\n", sal-(0.1*sal));
            }
            else
            {
                printf ("\nSalario com imposto descontado: R$%.2f\n", sal-(0.15*sal));
            }
        break;
        }
        case 'B':
        {
            if (sal<1000)
            {
                printf ("\nSalario aumentado: R$%.2f\n", sal+75);
            }
            else if (sal>=1000 && sal<1500)
            {
                printf ("\nSalario aumentado: R$%.2f\n", sal+100);
            }
            else
            {
                printf ("\nSalario aumentado: R$%.2f\n", sal+150);
            }
        break;
        }
        case 'b':
            {
            if (sal<1000)
            {
                printf ("\nSalario aumentado: R$%.2f\n", sal+75);
            }
            else if (sal>=1000 && sal<1500)
            {
                printf ("\nSalario aumentado: R$%.2f\n", sal+100);
            }
            else
            {
                printf ("\nSalario aumentado: R$%.2f\n", sal+150);
            }
        break;
        }
        case 'C':
        {
            if (sal>=1000)
            {
                printf ("\nCategoria A.\n");
            }
            else
            {
                printf ("\nCategoria B.\n");
            }
        break;
        }
        case 'c':
        {
            if (sal>=1000)
            {
                printf ("\nCategoria A.\n");
            }
            else
            {
                printf ("\nCategoria B.\n");
            }
        break;
        }
        default:
            printf ("\nFavor selecionar uma opcao valida.\n");
    }
    return 0;
}
