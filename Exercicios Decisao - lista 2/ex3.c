/*3) Ler tr�s valores inteiros diferentes e coloc�-los em ordem crescente. Os valores devem ser apresentados
com uma instru��o:*/

#include <stdio.h>
#include <stdlib.h>

int main (void)
{
    int v1, v2, v3, maior, menor, meio;

    printf ("Insira um valor inteiro:");
    scanf ("%d", &v1);
    printf ("Insira outro valor inteiro:");
    scanf ("%d", &v2);
    printf ("Insira outro valor inteiro:");
    scanf ("%d", &v3);

    if (v1<v2 && v2<v3)
    {
        menor = v1;
        meio = v2;
        maior = v3;
        printf ("\nOrdem Crescente: %d, %d, %d.\n", menor, meio, maior);
    }
    else
    {
        if (v1<v3 && v3<v2)
        {
            menor = v1;
            maior = v2;
            meio = v3;
            printf ("\nOrdem Crescente: %d, %d, %d.\n", menor, meio, maior);
        }
        else
        {
            if (v2<v1 && v1<v3)
            {
                meio = v1;
                menor = v2;
                maior = v3;
                printf ("\nOrdem Crescente: %d, %d, %d.\n", menor, meio, maior);
            }
            else
            {
                if (v2<v3 && v3<v1)
                {
                    maior = v1;
                    menor = v2;
                    meio = v3;
                    printf ("\nOrdem Crescente: %d, %d, %d.\n", menor, meio, maior);
                }
                else
                {
                    if (v3<v1 && v1<v2)
                    {
                        meio = v1;
                        maior = v2;
                        menor = v3;
                        printf ("\nOrdem Crescente: %d, %d, %d.\n", menor, meio, maior);
                    }
                    else
                    {
                        if (v3<v2 && v2<v1)
                        {
                            maior = v1;
                            meio = v2;
                            menor = v3;
                            printf ("\nOrdem Crescente: %d, %d, %d.\n", menor, meio, maior);
                        }
                        else
                        {
                            if (v1==v2 && v2==v3)
                            {
                                printf ("\nOs valores sao iguais. Portanto, nao ha ordem crescente.\n");
                            }
                        }
                    }
                }
            }
        }
    }
    return 0;
}
