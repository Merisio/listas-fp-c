/*6) Ler um n�mero inteiro longo (long int) que representa os segundos e convert�-lo para horas, minutos e
segundos. Mostrar a quantidade de horas, minutos e segundos obtidos, no seguinte formato:
xhoras:yminutos:zsegundos.*/

#include <stdio.h>

int main (void)
{
    long int num;
    int hor, min, seg;

    printf ("Informe um tempo em segundos:");
    scanf ("%ld", &num);

    hor = (num/3600);
    min = (num%3600)/60;
    seg = (num%3600)%60;

    printf ("\n%.0d Horas, %.0d Minutos, %.0d Segundos.\n", hor, min, seg);

    return 0;
}
