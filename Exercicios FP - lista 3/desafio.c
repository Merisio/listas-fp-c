/*10) (DESAFIO) Obter o resto da divis�o de dois valores inteiros informados pelo usu�rio, sem usar o operador
de resto.*/

#include <stdio.h>

int main(void)
{
    int num1, num2, div, res;

    printf("Informe um valor: ");
    scanf("%d", &num1);
    printf("Informe outro valor: ");
    scanf("%d", &num2);

    div = num1/num2;
    res = num1 - (div*num2);

    printf ("\nO resto da divisao eh: %.d\n", res);

    return 0;
}
