/*8) Ler um n�mero que representa a quantidade de dias. Informe os anos (considere-os com 360 dias), meses
(considere-os com 30 dias) e os dias contidos nesse valor.*/

#include <stdio.h>

int main(void)
{
    long int num;
    int anos, meses, dias;

    printf("Informe o tempo em dias:");
    scanf("%li", &num);

    anos = num/360;
    meses = (num%360)/30;
    dias = ((num%360)%30)%7;

    printf ("\nEsse tempo equivale a:\n");
    printf ("\n%.0d Anos", anos);
    printf ("\n%.0d Meses", meses);
    printf ("\n%.0d Dias\n", dias);

    return 0;
}
