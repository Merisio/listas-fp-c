/*4) Fazer um programa que leia um n�mero inteiro de at� tr�s d�gitos (considere que ser� fornecido um
n�mero de at� 3 d�gitos), calcule e imprima a soma dos seus d�gitos. Exemplo:*/

#include <stdio.h>

int main (void)
{
    int totp, presp;
    float valt, valp, sald;

    printf ("Informe a quantidade total de prestacoes:");
    scanf ("%d", &totp);
    printf ("Informe a quantidade de prestacoes pagas:");
    scanf ("%d", &presp);
    printf ("Informe o valor da prestacao:");
    scanf ("%f", &valt);

    valp = presp*valt;
    sald = (totp*valt) - valp;

    printf ("\nValor total ja pago: %.2f", valp);
    printf ("\nSaldo devedor: %.2f\n", sald);

    return 0;
}
