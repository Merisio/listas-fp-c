/*7) Suponha que um caixa disponha apenas de c�dulas de R$ 100, R$ 10 e R$ 1. Escreva um programa para
ler o valor de uma compra e o valor fornecido pelo usu�rio para pagar essa compra, e calcule o troco.
Calcular e mostrar a quantidade de cada tipo de c�dula que o caixa deve fornecer como troco. Mostrar
tamb�m o valor da compra e do troco. Use vari�veis do tipo int.*/

#include <stdio.h>

int main (void)
{
    int vp, vc, t, t1, t10, t100;

    printf ("Insira o valor de compra:");
    scanf ("%d", &vc);
    printf ("Insira o valor pago:");
    scanf ("%d", &vp);

    t = vp-vc;
    t100 = t/100;
    t = t - (t100*100);
    t10 = t/10;
    t = t - (t10*10);
    t1 = t;

    printf ("\nO troco foi pago com:");
    printf ("\n%.0d notas de 100 reais", t100);
    printf ("\n%.0d notas de 100 reais", t10);
    printf ("\n%.0d notas de 100 reais\n", t1);

    return 0;
}
