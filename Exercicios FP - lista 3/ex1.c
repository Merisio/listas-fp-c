/*1) Ler um n�mero double. Separar a parte inteira e a parte decimal desse n�mero. Apresentar a parte decimal
como um valor double e como um inteiro de tr�s d�gitos. Da parte inteira separar o n�mero que representa
unidade, dezena e centena e mostrar.*/

#include <stdio.h>

int main(void)
{
    double num, dec;
    int inteiro, decint, c, d, u;

    printf("Informe um valor double:");
    scanf("%lf", &num);

    inteiro = (int)num;
    dec = num - (double)inteiro;
    decint = dec*(int)1000;
    c = inteiro/100;
    d = (inteiro%100)/10;
    u = (inteiro%100)%10;

    printf("\nNumero: %lf\n", num);
    printf("Parte inteira: %d\n", inteiro);
    printf("Parte decimal: %lf\n", dec);
    printf("Parte decimal como inteiro de tres digitos: %d\n", decint);
    printf("Centenas: %d\n", c);
    printf("Dezenas: %d\n", d);
    printf("Unidades: %d\n", u);

    return 0;

}
