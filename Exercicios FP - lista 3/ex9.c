/*9) Criar um programa que leia o peso (em kg) de uma pessoa e calcule e imprima:
a) O peso da pessoa em gramas.
b) O novo peso, em gramas, se a pessoa engordar 12%.*/

#include <stdio.h>

int main(void)
{
    float kg, gr, kg12, gr12;

    printf("Informe o seu peso (em kg):");
    scanf("%f", &kg);

    gr = kg*1000;
    gr12 = ((gr * 12)/100) + gr;
    kg12 = ((kg *12)/100) + kg;

    printf("\nPeso em gramas: %.1f\n", gr);
    printf("Peso em gramas se engordar 12%%: %.1f\n", gr12);
    printf("Peso em kg se engordar 12%%: %.1f\n", kg12);

    return 0;
}
