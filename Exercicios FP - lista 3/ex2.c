/*2) Considerando que para um cons�rcio sabe-se o n�mero total de presta��es, a quantidade de presta��es
pagas e o valor de cada presta��o (que � fixo). Escreva um programa que determine o valor total j� pago pelo
consorciado e o saldo devedor.*/

#include <stdio.h>

int main (void)
{
    int totp, presp;
    float valt, valp, sald;

    printf ("Informe a quantidade total de prestacoes:");
    scanf ("%d", &totp);
    printf ("Informe a quantidade de prestacoes pagas:");
    scanf ("%d", &presp);
    printf ("Informe o valor da prestacao:");
    scanf ("%f", &valt);

    valp = presp*valt;
    sald = (totp*valt) - valp;

    printf ("\nValor total ja pago: %.2f", valp);
    printf ("\nSaldo devedor: %.2f\n", sald);

    return 0;
}
