/*5) Fa�a um programa para calcular a quantidade de fita necess�ria, em cent�metros, para amarrar um pacote
com duas voltas, sendo uma pela largura e outra pelo comprimento do pacote. Ser�o fornecidas a largura, a
altura e o comprimento do pacote. Para amarrar as pontas da fita s�o necess�rios 15 cm de fita em cada
ponta. A figura a seguir ilustra a maneira como a fita � passada pelo pacote.*/

#include <stdio.h>

int main (void)
{
    float alt, larg, comp, tamf;

    printf ("Informe a altura da caixa em cm:");
    scanf ("%f", &alt);
    printf ("Informe a largura da caixa em cm:");
    scanf ("%f", &larg);
    printf ("Informe o comprimento da caixa em cm:");
    scanf ("%f", &comp);

    tamf = ((2*alt)+(2*comp)) + ((2*alt)+(2*larg)) + 30.0;

    printf ("\nSerao necessarios %.1f de fita\n", tamf);

    return 0;
}
