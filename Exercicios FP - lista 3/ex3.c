/*3) Leia um valor double que representa o troco a ser fornecido por um caixa. Separe a parte inteira (reais) da
parte decimal (centavos) e apresente na forma: 123 reais e 18 centavos. Use a fun��o round, da biblioteca
math.h, para o arredondamento da parte decimal).*/

#include <stdio.h>
#include <math.h>

int main (void)
{
    double valtroc;
    int reais, cents;

    printf ("Informe o valor do troco (em reais):");
    scanf ("%lf", &valtroc);

    reais = (int)valtroc;
    cents = (valtroc-reais)*100;


    printf ("\nO valor informado eh %.0d reais e %.0d centavos.", reais, cents);

    return 0;
}
